use std::cell::RefCell;
use std::future::Future;
use std::pin::Pin;
use std::rc::Rc;
use std::task::{Context, Poll, RawWaker, RawWakerVTable, Waker};

fn dummy_raw_waker() -> RawWaker {
    fn no_op(_: *const ()) {}
    fn clone(_: *const ()) -> RawWaker {
        dummy_raw_waker()
    }

    let vtable = &RawWakerVTable::new(clone, no_op, no_op, no_op);
    RawWaker::new(std::ptr::null::<()>(), vtable)
}

fn dummy_waker() -> Waker {
    unsafe { Waker::from_raw(dummy_raw_waker()) }
}

struct Task {
    future: Pin<Box<dyn Future<Output = ()>>>,
    completed: bool,
}

impl Task {
    pub fn new(future: impl Future<Output = ()> + 'static) -> Task {
        Task {
            future: Box::pin(future),
            completed: false,
        }
    }

    fn poll(&mut self, context: &mut Context) -> Poll<()> {
        self.future.as_mut().poll(context)
    }
}

#[derive(Clone, Default)]
struct SharedState {
    completed: bool,
}

#[derive(Clone, Default)]
struct Timer {
    time: u32,
    state: Rc<RefCell<SharedState>>,
}

#[derive(Default)]
struct WorldInner {
    clock: RefCell<u32>,
    task_queue: RefCell<Vec<Task>>,
    timers: RefCell<Vec<Timer>>,
}

#[derive(Clone, Default)]
pub struct World {
    inner: Rc<WorldInner>,
}

impl World {
    fn delay(&self, amount: u32) -> TimerFuture {
        let shared_state = Rc::new(RefCell::new(SharedState { completed: false }));
        let timer = Timer {
            time: *self.inner.clock.borrow() + amount,
            state: shared_state.clone(),
        };
        self.inner.timers.borrow_mut().push(timer);
        TimerFuture {
            state: shared_state,
        }
    }

    fn spawn(&self, future: impl Future<Output = ()> + 'static) {
        self.inner.task_queue.borrow_mut().push(Task::new(future));
    }

    fn update_timers(&mut self) {
        let timers = self.inner.timers.borrow();
        for timer in timers.iter() {
            // println!("timer: {}", timer.time);
            if timer.time <= *self.inner.clock.borrow() {
                (*(timer.state)).borrow_mut().completed = true;
            }
        }
    }

    fn step(&mut self) {
        self.update_timers();

        let waker = dummy_waker();
        let context = &mut Context::from_waker(&waker);
        let mut tasks = self.inner.task_queue.borrow_mut();
        let mut uncompleted_tasks = Vec::new();
        for mut task in tasks.drain(..) {
            // println!("Polling...");
            match task.poll(context) {
                Poll::Ready(()) => {
                    task.completed = true;
                    println!("Task DONE")
                } // task done
                Poll::Pending => {
                    // println!("Task is pending");
                    uncompleted_tasks.push(task);
                }
            }
        }
        *tasks = uncompleted_tasks;
        *self.inner.clock.borrow_mut() += 1;
    }
}

struct TimerFuture {
    state: Rc<RefCell<SharedState>>,
}

impl Future for TimerFuture {
    type Output = ();
    fn poll(self: Pin<&mut Self>, _cx: &mut Context) -> Poll<Self::Output> {
        // println!("Polling timer...");
        if self.state.borrow().completed {
            // println!("Timer ready");
            Poll::Ready(())
        } else {
            // println!("Timer pending");
            Poll::Pending
        }
    }
}

async fn start_vm1(world: World) {
    println!("Starting VM 1");
    world.delay(3).await;
    println!("Started VM 1");
    world.delay(5).await;
    println!("Stopped VM 1");
}

async fn start_vm2(world: World) {
    println!("Starting VM 2");
    world.delay(1).await;
    println!("Started VM 2");
    world.delay(4).await;
    println!("Stopped VM 2");
}

fn main() {
    let mut world = World::default();
    world.spawn(start_vm1(world.clone()));
    world.step();
    world.spawn(start_vm2(world.clone()));
    for _ in 0..10 {
        println!("-- step {}", *world.inner.clock.borrow());
        world.step();
    }
}
